import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { Product } from '../../../types/product.inteface';
import { By } from '@angular/platform-browser';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  //Configurações que são executadas antes de cada um dos testes do componente
  beforeEach(async () => {
    //Dá ao contexto (componente), tudo que precisa ser inicializado para a execução do teste, faz a preparação de serviços e dependências necessárias para testar o componente
    await TestBed.configureTestingModule({
      imports: [CardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve renderizar o produto no template', () => {
    //Criar o mock do produto
    const product: Product = {
      id: 1,
      title: 'IPhone 15',
      price: 'U$ 1000',
      category: 'Eletrônico',
      description: 'Smart Phone',
      image: 'assets/phones/phone.png'
    };

    component.product = product;

    //Após fazer o mock e passá-lo ao componente, é preciso dizer para o componente detectar as mudanças que chegaram no Input, por isso é importante utilizar o método detectChanges()
    fixture.detectChanges();

    const productImg = fixture.debugElement.query(By.css('img')).nativeElement;
    const productTitle = fixture.debugElement.query(By.css('h2')).nativeElement;
    const productDescription = fixture.debugElement.query(By.css('p')).nativeElement;
    const productPrice = fixture.debugElement.query(By.css('h3')).nativeElement;

    expect(productImg.src).toContain(product.image);
    expect(productTitle.textContent).toContain(product.title);
    expect(productDescription.textContent).toContain(product.description);
    expect(productPrice.textContent).toContain(product.price)
  });

  it('deve emitir o evento onDelete quando clicar no botão Delete', () => {    
    //Criar o mock do produto
    const product: Product = {
      id: 2,
      title: 'Samsung S22',
      price: 'U$ 750',
      category: 'Eletrônico',
      description: 'Smart Phone Samsung',
      image: 'assets/phones/phone_samsung.png'
    };

    component.product = product;
    fixture.detectChanges();

    //Spy (espiões) alteram o comportamento de um método ou função dentro do contexto do teste
    //Sintax -> spyOn(contexto(ou componente).método, 'nome da função a se espionar, de fato'), ela retorna a função com um espião dentro dela
    const spy = spyOn(component.onDelete, 'emit');

    component.isManagable = true;
    fixture.detectChanges();

    const managableElement = fixture.debugElement.query(By.css('span')).nativeElement;
    expect(managableElement).not.toBeNull();

    component.onDeleteClick();
    expect(spy).toHaveBeenCalledWith(product);
  });

  it('deve emitir o evento onEdit quando clicar no botão Edite', () => {    
    //Criar o mock do produto
    const product: Product = {
      id: 3,
      title: 'Samsung A20',
      price: 'U$ 730',
      category: 'Eletrônico',
      description: 'Outro Smart Phone Samsung',
      image: 'assets/phones/outro_phone_samsung.png'
    };

    component.product = product;
    fixture.detectChanges();

    const spy = spyOn(component.onEdit, 'emit');

    component.isManagable = true;
    fixture.detectChanges();

    const managableElement = fixture.debugElement.query(By.css('span')).nativeElement;
    expect(managableElement).not.toBeNull();

    component.onEditClick();
    expect(spy).toHaveBeenCalledWith(product);
  });

  // it('', () => {

  // });
});
