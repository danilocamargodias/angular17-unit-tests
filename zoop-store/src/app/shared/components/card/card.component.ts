import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Product } from '../../../types/product.inteface';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [
    CommonModule,
    MatIconModule
  ],
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss'
})
export class CardComponent {

  /*
  Nem sempre é necessário testar os decorators, mas quando podem assumir múltiplos valores, como Product ou Null, então torna-se importante testar ambas as situações.
  */
  @Input()
  product: Product | null = null;

  @Input()
  isManagable = false;

  @Output() onDelete = new EventEmitter<Product | null>();

  @Output() onEdit = new EventEmitter<Product | null>();

  onDeleteClick(): void {
    this.onDelete.emit(this.product)
  }

  onEditClick(): void {
    this.onEdit.emit(this.product)
  }
}
